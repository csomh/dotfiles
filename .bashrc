# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

export PATH="$HOME/bin:$HOME/.local/bin:$PATH"

# enabling some handy options
shopt -s globstar

# User specific aliases and functions
alias gl='git log -10'
alias gf='git fetch --all'
alias gr='git remote -v'
alias gs='git status -s -b'
alias ts='tig status'
alias gp='git pull'
alias gd='git diff --patience'
alias gg='git grep'
alias gcl="git clean -e 'tags' -e 'TODO.csomh.md' -xd"
alias gitroot='git rev-parse --show-toplevel'
alias g='git'

if ( command -v minikube &>/dev/null ); then
    source <(minikube completion bash)
    alias mk="minikube"
    complete -o default -F __start_minikube mk
fi

if ( command -v kubectl &>/dev/null ); then
    source <(kubectl completion bash)
    alias k="kubectl"
    complete -o default -F __start_kubectl k
fi


if [ -f /usr/share/bash-completion/completions/git ]; then
    source /usr/share/bash-completion/completions/git
    __git_complete gb _git_branch
    __git_complete gl _git_log
    __git_complete gfl _git_log
    __git_complete gll _git_log
    __git_complete gd _git_diff
    __git_complete gcl _git_clean
    __git_complete g __git_main
fi

alias ll='ls -l --human-readable --color=auto' 2>/dev/null

alias wget-dir="wget --no-directories --no-host-directories --recursive --level=1 --no-parent"
alias cd.="cd ../"
alias cd..="cd ../../"
alias cd...="cd ../../../"
alias cd....="cd ../../../../"
alias cd.....="cd ../../../../../"
alias todo="rg 'TODO\(csomh\)'"

ptags() {
    ctags --python-kinds=-i $(git ls-files)
}

clip() {
    xclip -i -sel clipboard $@
}

if [ -f /usr/share/git-core/contrib/completion/git-prompt.sh ]; then
    source /usr/share/git-core/contrib/completion/git-prompt.sh
    export GIT_PS1_SHOWDIRTYSTATE=1
    export GIT_PS1_SHOWSTASHSTATE=1
    export GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWUPSTREAM="verbose name"

    GITINFO='$(__git_ps1 "(%s)")'
fi

set_prompt() {
    local ex=$?
    if [[ "$ex" -ne 0 ]]; then
        ex="\[\e[31m[${ex}]\] "
    else
        ex=""
    fi

    if [ -n "$VIRTUAL_ENV" ]; then
        venv="\[\e[33m($(basename $VIRTUAL_ENV))\] "
    else
        venv=""
    fi

    # Change the prompt if in a toolbox
    if [ -n "$NAME" ]; then
        prompt=📦
    else
        prompt=🧢
    fi
    export PS1="${venv}\[\e[34m\w\]\[\e[94m ${GITINFO}\]\n${ex}${prompt} \[\e[0m\]"
}

# avoid adding set_prompt multiple times
if [[ $PROMPT_COMMAND != *"set_prompt"* ]]; then
    # set_prompt should come first, in order to be able to capture error
    # codes
    export PROMPT_COMMAND="set_prompt ${PROMPT_COMMAND:+; $PROMPT_COMMAND}"
fi

# python virtenv
if [ -f /usr/bin/virtualenvwrapper.sh ];then
    source /usr/bin/virtualenvwrapper.sh
    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/Envs
fi

if [ -e /usr/bin/nvim ]; then
    alias vim='nvim'
elif [ -e /usr/bin/vimx ]; then
    alias vim='vimx'
fi

export EDITOR='nvim'
alias e="$EDITOR"
export FZF_DEFAULT_COMMAND="fd --hidden --no-ignore"
export FZF_DEFAULT_OPTS="--height=30% --layout=reverse --cycle"
fe() {
    selection=$(fzf -i -1 -q "$*")
    if [ $? -eq 0 ]; then
        $EDITOR "$selection"
    fi
}

re() {
    match=$(rg --line-number "$*" | fzf -i -1)
    location=$(echo $match | sed -E 's/^(.+):([0-9]+):.+$/\1 +\2/')
    if [ $? -eq 0 ]; then
        $EDITOR $location
    fi
}

dj() {
    local directory=$(fd --max-depth 3 --type directory . ~/src | fzf -q "$*")
    if [ -n "$directory" ]; then
        cd "$directory"
    fi
}

gb() {
    if [[ "${1}" != -* ]]; then
        output=$(git branch | fzf --print-query --query "${*:-}")
        result=$?
        if [ $result -eq 1 ] ; then
            branch="-c "$(echo "$output" | head -1)
        elif [ $result -eq 0 ] ; then
            branch=$(echo "$output" | tail -1 | tr -d '*')
        else
            return $result
        fi
        git switch $branch
    else
        git branch $*
    fi
}

alias xo=xdg-open
