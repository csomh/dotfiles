" let g:molokai_original = 1
syntax enable
" colorscheme monokai
let g:gruvbox_italic = '1'
colorscheme gruvbox
set background=dark


" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2016 Apr 05
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  set undofile		" keep an undo file (undo changes after closing)
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
  syntax on

  " Also switch on highlighting the last used search pattern.
  set hlsearch

  " I like highlighting strings inside C comments.
  let c_comment_strings=1
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text,markdown,rst,asciidoc setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

  autocmd FileType yaml setlocal ai ts=2 sw=2 et
  autocmd FileType gitcommit,markdown,rst,python,asciidoc setlocal spell
  autocmd FileType python setlocal formatprg=black\ \-\-fast\ \-q\ \-
  " autocmd BufWritePre * :%s/\s\+$//e

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If unset (default), this may break plugins (but it's backward
  " compatible).
  set langnoremap
endif


" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
packadd matchit

let mapleader=' '
noremap <Space> <Nop>

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
vnoremap . :normal .<CR>
set cursorline
" set spell
" set clipboard=unnamedplus
noremap <Leader>y "+y
noremap <Leader>p "+p
noremap <Leader>P "+P
noremap <Leader>gf :GFiles<CR>
noremap <Leader>ff :Files<CR>
noremap <Leader>rf :History<CR>
noremap <Leader>gs :Git<CR>
noremap <Leader>rr :Rg<CR>
noremap <Leader>bb :Buffers!<CR>
noremap <Leader>bn :bn<CR>
noremap <Leader>bp :bp<CR>
noremap <Leader>bd :bd<CR>
noremap <Leader>bf :!black %<CR>
noremap <Leader>t :BTags<CR>
noremap <Leader>gt :Tags<CR>
noremap <Leader>/ :Commentary<CR>
noremap <Leader>mh1 :Mdh1<CR>
noremap <Leader>mh2 :Mdh2<CR>
noremap <Leader>mh3 :Mdh3<CR>
noremap <Leader>fs :w<CR>
" mapping for window manipulation
noremap <Leader>wv :vsplit<CR>
noremap <Leader>ws :split<CR>
noremap <Leader>wd <C-W>c
noremap <Leader>w<Left> <C-W><Left>
noremap <Leader>w<Right> <C-W><Right>
noremap <Leader>w<Down> <C-W><Down>
noremap <Leader>w<Up> <C-W><Up>
noremap <Leader>q :q<CR>

nnoremap <Leader>gb :.GBrowse@origin<CR>
vnoremap <Leader>gb :GBrowse@origin<CR>
vnoremap <Leader>gbb :GBrowse<CR>

" Set as groovy anything that looks like a Jenkinsfile
au BufNewFile,BufRead Jenkinsfile*,*.Jenkinsfile setf groovy

" Have context sensitive relative line numbers
set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" always show the status line
set laststatus=2

" more natural splitting
set splitbelow
set splitright

set dir=~/.vim/swap//
set backupdir=~/.vim/backups//
set undodir=~/.vim/undo

function! CopyMatches(reg)
  let hits = []
  %s//\=len(add(hits, submatch(0))) ? submatch(0) : ''/gne
  let reg = empty(a:reg) ? '+' : a:reg
  execute 'let @'.reg.' = join(hits, "\n") . "\n"'
endfunction
command! -register CopyMatches call CopyMatches(<q-reg>)

"" Align line-wise comment delimiters flush left instead of following code
"" indentation
"let g:NERDDefaultAlign = 'left'
" Empty value to disable preview window altogether
let g:fzf_preview_window = ''

" Commands
command Date read !date --iso-8601 | tr -d '\n'
command Mdh1 normal I# <Esc>
command Mdh2 normal I## <Esc>
command Mdh3 normal I### <Esc>

" In order to be able to switch buffers without saving changes
set hidden

let g:airline_powerline_fonts = 1

" Folding in markdown
let g:markdown_folding = 1
au FileType markdown setlocal foldlevel=99
